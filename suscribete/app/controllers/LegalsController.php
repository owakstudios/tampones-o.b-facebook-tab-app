<?php

/**
 * Created by androide_osorio.
 * Date: 6/2/15
 * Time: 08:51
 */

class LegalsController extends BaseController {

    /**
     * render terms and conditions
     * @return \Illuminate\View\View
     */
    public function terms()
    {
        return View::make('legals.terms-and-conditions');
    }
}