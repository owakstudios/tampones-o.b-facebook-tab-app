<?php

/**
 * Created by androide_osorio.
 * Date: 5/28/15
 * Time: 16:10
 */

class UsersController extends BaseController {

    /**
     * Renders the registration form
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return View::make( 'users.create' );
    }

    /**
     * "creates" a new user or returns validation errors
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $userInput = Input::except('_token');
        $user = new User($userInput);

        if($user->save()) {
            // send email to SAC
            Event::fire('user.created', array($user));
            Event::fire('user.subscribed', array($user));

            return Redirect::route( 'users.welcome' )
                ->with('users_count', User::count())
                ->with( 'message', 'Gracias por Registrarte!' );
        }

        return Redirect::back()
                ->withInput()
                ->withErrors($user->errors());

    }

    /**
     * renders the welcome (users index) page
     */
    public function welcome()
    {
        return View::make( 'users.index' )->with('users_count', User::count());
    }
}