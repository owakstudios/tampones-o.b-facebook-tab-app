<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->index();
			$table->dateTime('dob');
			$table->string('gender');
			$table->string('document_type');
			$table->string('document_id')->index();
			$table->text('comment');
			$table->boolean('legals_tnc')->default(1)->index();
			$table->boolean('legals_privacy_policy')->default(0)->index();
			$table->boolean('legals_email_list')->default(0)->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
