<?php

class UserEventsHandler {

    /**
     * Handle user login events.
     */
    public function sendEmailConfirmation($user)
    {
        Mail::send('emails.users.subscription', $user->toArray(), function($message)
        {
            $message->to('consumidor@conco.jnj.com', 'SAC')
                    ->from('noreply@viveob.com', 'Nos pasa a Todas en Facebook')
                    ->subject('Nuevo usuario inscrito a través de facebook');
        });
    }

    /**
     * Handle user logout events.
     */
    public function subscribeWithSoap($event)
    {
        //TODO: implement SOAP client
        $soapClient = new PlusoftSoapClient();
        //dd($soapClient->functions());
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('user.created', 'UserEventsHandler@sendEmailConfirmation');

        $events->listen('user.subscribed', 'UserEventsHandler@subscribeWithSoap');
    }

}