<?php
use LaravelBook\Ardent\Ardent;


class User extends Ardent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
     * validation rules
     *
     * @var array
     */
	public static $rules = array(
		'name'          => 'required|min:6',
		'email'         => 'required|email|unique:users|confirmed',
		// 'document_type' => 'in:cc,ti,cex,pass',
		// 'document_id'   => 'unique:users|min:6',
		'gender'        => 'required|in:F,M,O,Masculino,Femenino,Otro',
		'legals_tnc'    => 'required'
   );

  /**
   * custom validation messages
   *
   * @var
   */
  public static $customMessages = array(
  	'name.required'          => 'Debes ingresar tu nombre completo.',
  	'name.min'               => 'Debes ingresar tu nombre completo.',
  	'email.required'         => 'Debes ingresar tu email.',
  	'email.email'            => 'Ingresa un e-mail valido (ejemplo:tu@ejemplo.com).',
  	'email.unique'           => 'Ya estas registrado(a) en nuestra lista',
  	'email.confirmed'        => 'La confirmación de tu e-mail no concuerda.',
  	// 'document_type.required' => 'Debes ingresar tu tipo de documento de identidad.',
  	// 'document_id.required'   => 'Debes ingresar tu documento de identidad.',
  	'document_id.min'        => 'Tu documento de identidad debe tener más de 6 caracteres.',
  	'document_id.unique'     => 'Ya hay una persona registrada con ese documento',
  	'gender.required'        => 'Debes ingresar tu género.',
  	'gender.in'              => 'No seleccionaste un género válido',
  	// 'comment.required'       => 'Debes tener algo qué contarnos ¿no?',
  	// 'comment.min'            => 'Seguramente tu inquietud o historia debe tener más de 10 caracteres',
  	'legals_tnc.required'    => 'Debes aceptar los términos y condiciones para inscribirte.'
    );

	//-----------------------------------------------------------------

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $guarded = array();

  /**
   * purge confirmation attributes
   * @var boolean
   */
  public $autoPurgeRedundantAttributes = true;

  public static $throwOnFind = true;

  public function beforeValidate()
  {
    if(!array_key_exists('dob', $this->attributes)) {
      $this->dob = $this->transformDate();
      unset($this->attributes[ 'year' ], $this->attributes[ 'month' ], $this->attributes[ 'day' ]);
    }

    if($this->isDirty('gender')) {
      $this->gender = $this->transformGender($this->gender);
    }

    return true;
  }

  protected function transformDate()
  {
    return Carbon\Carbon::createFromDate(
      $this->attributes[ 'year' ],
      $this->attributes[ 'month' ],
      $this->attributes[ 'day' ]
    );
  }

  protected function transformGender($genderIdentifier)
  {
    switch($genderIdentifier) {
      case 'M': return 'Masculino';
      break;
      case 'F': return 'Femenino';
      break;
      default: return 'Otro';
      break;
    }
  }
}