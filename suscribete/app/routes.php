<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * renders user subscription form
 */
Route::get( '/', array(
    'uses' => 'UsersController@create',
    'as'   => 'users.create'
) );

/**
 * route for "creating" new users
 */
Route::post( '/users/new', array(
    'uses' => 'UsersController@store',
    'as'   => 'users.store'
) );

/**
 * route for showing a message to a new registered user
 */
Route::get( '/enterate-de-nuestras-actividades-y-promociones', array(
    'uses' => 'UsersController@welcome',
    'as'   => 'users.welcome'
) );

/**
 * route for displaying terms and conditions
 */
Route::get( '/terminos-y-condiciones', array(
    'uses' => 'LegalsController@terms',
    'as'   => 'legals.terms-and-conditions'
) );

/**
 * test view to see if app is not broken
 */
Route::get( '/laravel', function ()
{
    return View::make('hello');
} );