<div class="column small-12 footer-section">
    <div class="column small-12 medium-12 large-11 small-centered">
        <div class="column small-12 medium-6 left-divider" id="follow-us">
            <img src="{{ URL::asset('public/images/icon-like.svg') }}" alt="LIKE" class="left icon-like" />
            <hgroup id="follow-us-title">
                <h3><strong>s&iacute;guenos</strong> <em>en nuestras</em></h3>
                <h2 style="font-size: 2.5rem;"><strong>redes sociales</strong></h2>
            </hgroup>
            <hr class="show-for-small-only" />
        </div>

        {{-- social media icons --}}
        <div class="column small-12 medium-6">
            <ul class="inline-list text-center social-media">
                <li>
                    <ul class="inline-list social-media-list">
                        <li><a href="https://facebook.com/nospasaatodas" target="_new"><i class="owak-icon-facebook owak-icon-green"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCpjhJwGv_93w2XKg4Zrmljg" target="_new"><i class="owak-icon-youtube owak-icon-green"></i></a></li>
                    </ul>
                    <small>/nospasaatodas</small>
                </li>
                <li>
                    <ul class="inline-list social-media-list">
                        <li><a href="https://twitter.com/secarefree" target="_new"><i class="owak-icon-twitter owak-icon-magenta"></i></a></li>
                        <li><a href="https://instagram.com/secarefree" target="_new"><i class="owak-icon-instagram owak-icon-magenta"></i></a></li>
                        <!--<li><a href="https://pinterest.com/" target="_new"><i class="owak-icon-pinterest owak-icon-magenta"></i></a></li>-->
                    </ul>
                    <small>/secarefree</small>
                </li>
                <li>
                    <ul class="inline-list social-media-list">
                        <li><a href="https://instagram.com/viveob" target="_new"><i class="owak-icon-instagram owak-icon-blue"></i></a></li>
                    </ul>
                    <small>/viveob</small>
                </li>
            </ul>
        </div>
        {{-- / social media icons --}}
    </div>
</div>

{{-- footer brands --}}
<div class="column small-12 footer-section">
    <div class="column small-10 medium-4 small-centered">
        <ul class="inline-list brand-list">
            <li>
                <figure class="secondary-brand">
                    <a href="http://secarefree.com" title="secarefree.com" target="_new">
                        <img src="{{ URL::asset('public/images/carefree-logo.png') }}" alt="CAREFREE®" />
                    </a>
                    <figcaption>
                        <a href="http://carefree.com.co" title="secarefree.com" target="_new">www.carefree.com.co</a>
                    </figcaption>
                </figure>
            </li>
            <li><h3>+</h3></li>
            <li>
                <figure class="secondary-brand">
                    <a href="http://secarefree.com" title="secarefree.com" target="_new">
                        <img src="{{ URL::asset('public/images/ob-logo.png') }}" alt="Tampones o.b®" />
                    </a>
                    <figcaption>
                        <a href="http://viveob.com" title="viveob.com" target="_new">www.viveob.com</a>
                    </figcaption>
                </figure>
            </li>
        </ul>
    </div>
</div>
{{-- /footer brands --}}
<div style="clear:both"></div>
{{-- legals --}}
<div class="column small-11 small-centered footer-section small-text-center medium-text-left">
    <p><small>&copy; Johnson & Johnson de Colombia S.A. 2015</small></p>
    <p>
        <small>
            Este e-mail se env&iacute;a de acuerdo a la informaci&oacute;n de tu registro en <a href="https://jnjcolombia.com">www.jnjcolombia.com</a>. Copyright de todo
            el contenido y de los art&iacute;culos en <a href="https://jnjcolombia.com">www.jnjcolombia.com</a>. Todos los derechos reservados.
            Los sitios de internet de Johnson & Johnson proveen informaci&oacute;n general, con prop&oacute;sito educativo solamente.
            Cualquier preocupaci&oacute;n o problema de salud, tuyo o de tu familia, requiere una consulta a un doctor o a otro
            profesional de la salud. Por favor revisa la pol&iacute;tica de privacidad y la nota legal antes de usar este sitio.
            Al usar el sitio indicas que estás de acuerdo y aceptas dichas pol&iacute;ticas.
        </small>
    </p>
</div>
{{-- / legals --}}
