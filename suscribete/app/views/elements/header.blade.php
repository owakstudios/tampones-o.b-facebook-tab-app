<header id="main-header">
    <h1 class="left">
        <img src="{{ URL::asset('public/images/nospasaatodas-logo@2x.png') }}" alt="Nos pasa a todas" class="brand" />
    </h1>

    <div class="left show-for-large-up" style="vertical-align:middle; min-height: 5rem;" >
        <img src="{{ URL::asset('public/images/divider.png') }}" alt="" style="margin-top: 2rem" />
    </div>

    <h2 class="right">
        <img src="{{ URL::asset('public/images/logo-carefree-ob.png') }}" alt="" />
    </h2>
    <div class="clear"></div>
</header>