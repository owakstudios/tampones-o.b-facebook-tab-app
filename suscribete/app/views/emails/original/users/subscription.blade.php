<?php setlocale(LC_ALL, 'es_ES') ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nuevo usuario suscrito</title>

    <style>
        @import url(http://fonts.googleapis.com/css?family=Lato:300,400,900);
        /* All your usual CSS here */
        
        body {
            font: normal normal 100%/1.5em 'Lato', Helvetica, sans-serif;
            background-color: #F5F5F5;
            padding: 10px;
        }
        p {
            color: #555555;
            font-weight: normal;
        }
        h1, h2, h3, h4, h5, h6 {
            font-weight: 900;
        }
        a {
            color: #4fdcc3;
            text-decoration:underline;
        }
        strong {
            font-weight: 900;
        }
        em {
            background-color: 34fdcc3;
            color: white;
        }
    </style>
</head>
<body>
<center>
    <img src="{{ url('public/images/nospasaatodas-logo@2x.png') }}" alt="NOS PASA A TODAS" />
</center>
<h1>Nuevo Usuario suscrito en <em>"Nos Pasa a Todas"</em> en Facebook</h1>

<p>Un nuevo usuario ha registrado sus datos a través de la aplicación de facebook en Nos Pasa a todas. La información de este usuario se encuentra abajo:</p>

<ul>
    <li><strong>nombre:</strong> {{ $name }}</li>
    <li><strong>Fecha de Nacimiento:</strong> {{ $dob->formatLocalized('%d %B, %Y') }} ({{$dob->age}} años)</li>
    <li><strong>Género:</strong> {{ $gender }}</li>
    <li><strong>Correo Electrónico:</strong> {{ $email }}</li>
</ul>

<h5>Nos comparte la siguiente historia:</h5>

<h5>El usuario(a):</h5>
<ul>
    @if(isset($legals_email_list))
        <li>Acepta que se le contacte via e-mail.</li>
    @else
        <li>No desea que se le contacte via e-mail.</li>
    @endif
</ul>

<small>Este es un mensaje automático, favor no responder.</small>
<img src="{{ url('public/images/logo-carefree-ob.png') }}" alt="CAREFREE-OB" />
</body>
</html>

