<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<?php setlocale(LC_ALL, 'es_ES') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Nuevo usuario suscrito</title>
</head>
<body style="background: #F5F5F5; padding: 10px; font: 100%/1.5em 'Lato', Helvetica, sans-serif;" bgcolor="#F5F5F5">
<center>
    <img src="{{ URL::asset('public/images/nospasaatodas-logo@2x.png') }}" alt="NOS PASA A TODAS">
</center>
<h1 style="font-weight: 900; text-transform-uppercase">Nuevo Usuario suscrito en <em style="color: #bf1e81;">"Nos Pasa a Todas"</em> en Facebook</h1>

<p style="color: #555555; font-weight: normal;">Un nuevo usuario ha registrado sus datos a través de la aplicación de facebook en Nos Pasa a todas. La información de este usuario se encuentra abajo:</p>

<ul>
    <li>
        <strong style="font-weight: 900;">nombre:</strong> {{ $name }}</li>
    <li>
        <strong style="font-weight: 900;">Fecha de Nacimiento:</strong> {{ $dob->formatLocalized('%d %B, %Y') }} ({{$dob->age}} años)</li>
    <li>
        <strong style="font-weight: 900;">Género:</strong> {{ $gender }}</li>
    <li>
        <strong style="font-weight: 900;">Correo Electrónico:</strong> {{ $email }}</li>
    <li>
        <strong style="font-weight: 900;">Fecha de suscripci&oacute;n:</strong> {{ strftime('%A, %d %B %Y, %H:%M') }}</li>

    </li>
</ul>
<h5 style="font-weight: 900;">Nos comparte la siguiente historia:</h5>

<h5 style="font-weight: 900;">El usuario(a):</h5>
<ul>
    @if(isset($legals_email_list))
        <li>Acepta que se le contacte via e-mail.</li>
    @else
        <li>No desea que se le contacte via e-mail.</li>
    @endif
</ul>
<small>Este es un mensaje automático, favor no responder.</small>
<br />
<center>
    <img src="{{ URL::asset('public/images/logo-carefree-ob.png') }}" alt="CAREFREE-OB">
</center>
</body>
</html>
