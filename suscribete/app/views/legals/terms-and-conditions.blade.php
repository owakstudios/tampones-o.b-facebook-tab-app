@extends('master')

@section('title') Términos y Condiciones del concurso @stop

@section('content')
    <div class="small-11 medium-8 column panel small-centered">
        <div class="text-center">
            <h3>JOHNSON &amp; JOHNSON DE COLOMBIA S.A.</h3>
            <h3>AVISO LEGAL - CAMPAÑA "HISTORIAS O.B®" – TERMINOS Y CONDICIONES</h3>
        </div>
        <p class="text-justify">
            Los términos y condiciones expuestos aquí, rigen la Campaña de “<strong>Historias sobre el periodo”</strong> propiedad de    <strong>JOHNSON &amp; JOHNSON DE COLOMBIA S.A. </strong>con NIT 890.101.815-9, sociedad con existencia legal y domicilio principal en Yumbo, Colombia, en
            adelante J&amp;J. Para aceptar estos términos haga clic en “Acepto”. Se entiende que si participa en la Campaña ha aceptado los términos y condiciones aquí
            expuestos.<strong> </strong>Para mayor información sobre nuestros servicios, puede visitar la siguiente página web:    <a href="http://www.jnjcolombia.com">www.jnjcolombia.com</a>; <a href="http://www.viveob.com.co">www.viveob.com.co</a> o contactarse a la línea gratuita a
            nivel nacional al 018000517000. Opcion 1
        </p>

        <p class="text-justify">
            <strong>MECÁNICA DE LA ACTIVIDAD: </strong>
            o.b.<sup>®</sup> invita a nuestras seguidoras en nuestro perfil de Facebook Nos Pasa A Todas :
        </p>
        <ol>
            <li>Darle “me gusta” a nuestra página de Facebook</li>
            <li>Llenar sus datos en la pestaña de la aplicación de Facebook</li>
            <li>Contarnos su historia relacionada con experiencias, mitos y dudas alrededor del periodo</li>
        </ol>

        <p class="text-justify">
            Serán premiadas diez (10) ganadoras quienes escriban las diez (10) mejores historias sobre experiencias, mitos y dudas alrededor del periodo menstrual. La
            escogencia la hará Johnson &amp; Johnson previa verificación que la participante sea seguidora de nuestra cuenta en Facebook Nos Pasa A Todas. El premio
            será un juego de tres (3) panties para cada una de las ganadoras.
        </p>
        <p class="text-justify">
            Las historias podrán ser usadas y modificadas en las redes sociales, página web y medios audiovisuales de Johnson &amp; Johnson, revelando la identidad de
            la concursante.
        </p>
        <p class="text-justify">
            <strong>PREMIOS:</strong>
        </p>
        <p class="text-justify">
            Las participantes ganadoras obtendrán como obsequio (3) panties de la talla que ellas soliciten(Small, médium o large). En total hay un stock de cien (100)
            unidades de diferentes tallas. Los premnios seran entregados asi:
        </p>

        <ol>
            <li>El centro de atención al consumidor se contactará con la ganadora a través de la información suministrad en la aplicación de Facebook de la actividad</li>
            <li>El centro de atención al consumidor solicitará a la ganadora la información adicional necesaria para hacer el envío de los panties en la talla
                solicitada por la consumidora.</li>
            <li>Si la ganadora no responde dentro de los tres (3) días hábiles siguientes a aquel en que fue contactada, el premio será reasignado a otra participante a
                elección de Johnson &amp; Johnson.</li>
            <li>El premio será enviado al domicilio indicado por la ganadora.</li>
        </ol>

        <p class="text-justify">
            <strong>FECHA DE INICIO DE LA ACTIVIDAD:</strong>
            5 de Junio 2015
        </p>
        <p class="text-justify">
            <strong>FECHA DE TERMINACIÓN DE LA ACTIVIDAD: 14 de Junio de 2014 </strong>
        </p>
        <p class="text-justify">
            <strong>PLAZO MÁXIMO PARA PUBLICAR LAS HISTORIAS: </strong>
            14 de Junio del 2014.
        </p>
        <p class="text-justify">
            <strong> </strong>
        </p>
        <p class="text-justify">
            <strong>FECHA DE ENTREGA DEL PREMIO: </strong>
            <strong>
                dentro de los diez (10) días hábiles siguientes a aquel en que sean recibidos los datos de la ganadora en el centro de atención del consumidor.
            </strong>
        </p>
        <p class="text-justify">
            <strong> </strong>
        </p>
        <p class="text-justify">
            <strong>PARTICIPACIÓN: </strong>
            Podrán participar gratuitamente todas las personas naturales (excluidas Personas Jurídicas), que cumplan las siguientes condiciones: <strong></strong>
        </p>

        <ul>
            <li>Ser mayores a 14 años</li>
            <li>Ser mujeres</li>
            <li>Estar residenciada en Colombia</li>
            <li>No ser empleadas de Johnson &amp; Johnson, o personas vinculadas a Johnson &amp; Johnson con contrato de prestación de servicios, ni parientes de los
                anteriores en primero o segundo grado de consanguinidad, segundo grado de afinidad o único civil. En caso de verificarse que una de las participantes se
                encontraba bajo esta situacion se anulará su participación.</li>
            <li>Cumplir con las demás condiciones indicadas en la mecánica de la actividad arriba indicadas, incluyendo las formas y los plazos.</li>
            <li>Cada persona podrá participar una sola vez.</li>
        </ul>

        <h4>PARTICIPACIONES MENORES DE QUINCE (15) AÑOS DE EDAD. </h4>

        <p class="text-justify">
            Este sitio web sólo puede ser usado por mayores de 15 años. Johnson &amp; Johnson no recopilará información de personas que no cumplan con este requisito a
            menos que la concursante demuestre tener autorización de sus padres, acudientes o guardadores.
        </p>
        <p class="text-justify">
            Johnson &amp; Johnson alienta y anima a los padres a hablar con sus hijas acerca de su uso de las redes sociales (Facebook, Twitter, Instagram y Facebook)
            y de la información que ellos revelan a través de estas redes sociales. Johnson &amp; Johnson hace todo lo que está a su alcance para evitar que se
            registren personas menores de 15 años. Si usted es menor de 15 años, no intente registrarse en este sitio, ni cargue su perfil de Twitter, Facebook e
            Instagram de Johnson &amp; Johnson. <strong>NO</strong> nos envie su fotografía, ni información personal como nombre, dirección, número de teléfono o
            correo electrónico.
        </p>
        <p class="text-justify">
            Si usted piensa que podemos tener cualquier tipo de información sobre alguien menor de 15 años de edad, por favor contáctenos por alguno de los medios
            descritos en estos términos y condiciones. Si identificamos que tenemos o hemos recibido información de una menor de 15 años de edad sin la correspondiente
            autorización de sus padres, esta será eliminada a la mayor brevedad.
        </p>
        <p class="text-justify">
            <strong>MODIFICACIONES: </strong>
            Johnson &amp; Johnson se reserva el derecho de modificar o revisar estos términos y condiciones en cualquier tiempo. En lo aquí no previsto se aplicará en
            lo dispuesto en las leyes vigentes. Johnson &amp; Johnson publicará las modificaciones en www.viveob.com.co las cuales rigen a partir de su publicación.    <strong></strong>
        </p>
        <p class="text-justify">
            <strong>RESTRICCIONES</strong>
            : <ins cite="mailto:Rodrigo" datetime="2014-12-10T15:19">Por el hecho de participar en esta campaña</ins>    <ins cite="mailto:Rodrigo" datetime="2014-12-10T15:20"> usted se abstendrá de: </ins>
        </p>
        <ol style="list-style-type:lower-alpha">
            <li>Disponer, licenciar, vender, ceder, alquilar, exportar, distribuir o transferir ni otorgar derechos a ningún tercero sobre la historia descrita por
                usted y compartida con nosotros.</li>
            <li>Alterar cualquier aviso de derecho de propiedad intelectual en los que se confirme la propiedad de J&amp;J sobre la historia y la presente Campaña
                promocional.</li>
            <li>Exponer ante terceros material ofensivo o de naturaleza censurable.</li>
        </ol>

        <p class="text-justify">
            <strong>USO:</strong>
            Para participar en la presente compaña Usted requiere tener una Cuenta o Usuario en Facebook y ser seguidora de nuestra cuenta    <strong><u>Nos Pasa a Todas. </u></strong>Usted es la única responsable de las actividades que se lleven a cabo a través de su Cuenta de Usuario y de
            mantener la confidencialidad y seguridad de la misma. J&amp;J no será responsable por ninguna pérdida derivada del uso no autorizado de su Cuenta. Usted se
            compromete a participar en la Campaña sólo para fines lícitos y conforme con las leyes vigentes de Colombia.
        </p>
        <p class="text-justify">
            <strong>DERECHOS DE PROPIEDAD</strong>
            : Usted acepta que las historias que usted nos comparte, los gráficos, interfaces de usuario, clips de audio, clips de video, contenido editorial, guiones
            y códigos utilizados para la presente Campaña promocional, así como red de Facebook e instagram de J&amp;J, contienen información que pasa a ser propiedad
            de J&amp;J, y que se encuentra protegido por las leyes aplicables de propiedad intelectual y demás aplicables.
        </p>
        <p class="text-justify">
            <strong> </strong>
        </p>
        <p class="text-justify">
            <strong>CARGOS</strong>
            : J&amp;J no le solicitará dinero, ni inversiones o compra de tarjetas por su participación en la Campaña. J&amp;J tampoco le deberá pagar a Usted ningún
            cargo o valor por su participación.
        </p>
        <p class="text-justify">
            <strong> </strong>
        </p>
        <p class="text-justify">
            <strong>DATOS PERSONALES</strong>
            : J&amp;J protege el habeas data de sus seguidores, clientes, proveedores, empleados y colaboradores. Para el efecto J&amp;J tiene un banco de datos
            actualizado y fidedigno que cumple con los estándares legales. Procesaremos su información personal y el contenido de sus comunicaciones conforme con
            nuestra Política de Privacidad y manejo de datos personales: www.jnjcolombia.com/contactenos-inicio?continue=contactenos-inicio
        </p>
        <p class="text-justify">
            <strong> </strong>
        </p>
        <p class="text-justify">
            <strong>TERMINACIÓN</strong>
            : En caso de que Usted incumpla alguno de los términos y condiciones aquí expuestos, Johnson &amp; Johnson tendrá el derecho de eliminar su imagen de la
            actividad a la vez que impedir su ingreso a nuestra red de Facebook e Instagram en un futuro.
        </p>
        <p class="text-justify">
            <strong> </strong>
        </p>
        <p class="text-justify">
            <strong>INDEMNIDAD</strong>
            : Usted está de acuerdo en indemnizar y amparar a Johnson &amp; Johnson de cualquier pérdida, responsabilidad civil, reclamo o demanda, incluyendo
            honorarios razonables de abogados, que haga cualquier tercero debido al uso de la Aplicación.
        </p>
        <p class="text-justify">
            <strong> </strong>
        </p>
        <p class="text-justify">
            <strong>DECLARACIONES FINALES: </strong>
            J&amp;J declara y garantiza que cuenta con todos los derechos y autorizaciones aplicables para el desarrollo de esta actividad. <strong></strong>
        </p>
        <p class="text-justify">
            La concursante que participe en la presente campaña promocional manifiesta que lo hace voluntariamente y autoriza a Johnson &amp; Johnson para difundir y
            publicar su nombre, imagen en el Facebook e Instagram de propiedad de J&amp;J, página web de la promoción (en caso de que exista) o donde y por el medio
            que J&amp;J considere, sin derecho a compensación o remuneración de ningún tipo por dicha publicación. Los participantes autorizan a J&amp;J a utilizar y
            editar su historia en caso de ser seleccionada para fines de la marca.
        </p>
        <p class="text-justify">
            J&amp;J se reserva el derecho de poner término o modificar la presente actividad, o descalificar a uno o varios Participantes, en caso de existir fraudes o
            sospechas de fraudes, dificultades técnicas o cualquier otro factor fuera de control de esta organización que pudieran comprometer la integridad de la
            actividad o que atente contra el espíritu promocional, competitivo y recreacional de ésta.
        </p>
        <p class="text-justify">
            J&amp;J no asumirá responsabilidad por el incumplimiento de sus obligaciones, cuando tal incumplimiento total o parcial se produzca por causas o
            circunstancias constitutivas de fuerza mayor o caso fortuito, calificadas de conformidad con la Ley.
        </p>
        <p class="text-justify">
            USTED RECONOCE EXPRESAMENTE QUE HA LEÍDO ESTOS TERMINOS Y CONDICIONES Y COMPRENDE LOS DERECHOS, LAS OBLIGACIONES Y CLÁUSULAS AQUÍ ESTIPULADAS. AL HACER
            CLIC EN EL BOTÓN "ACEPTAR", USTED ACEPTA EXPRESAMENTE REGIRSE POR ESTOS TERMINOS Y CONDICIONES SIN POSILIDAD A FUTURO RECLAMO, Y OTORGA A JOHNSON &amp;
            JOHNSON DE COLOMBIA S.A. LOS DERECHOS ESTABLECIDOS AQUÍ.
        </p>
    </div>
@stop