@extends('master')

@section('title') inscribete @stop

@section('content')
    <div class="column small-11 medium-8 small-centered email-form-panel">
        <h2>
            <strong style="font-style: italic;">Inscr&iacute;bete</strong>&nbsp;y haz parte<br>
            de <strong>todo</strong> lo que<br>
            <strong>tenemos para tí</strong>
        </h2>
        <p class="emphasized-text">Noticias, promociones, consejos y contenido exclusivo para tí.</p>

        @include('elements.subscription-form')
    </div>
@stop
